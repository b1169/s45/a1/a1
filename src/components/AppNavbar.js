/*import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav"*/

import { Navbar, Nav } from 'react-bootstrap';

function AppNavbar(){

	return(

/*// Navbar from React-Bootstrap

		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link href="#home">Home</Nav.Link>
		        <Nav.Link href="#link">Link</Nav.Link>
		        <NavDropdown title="Dropdown" id="basic-nav-dropdown">
		          <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
		          <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
		          <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
		          <NavDropdown.Divider />
		          <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
		        </NavDropdown>
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>*/

		<Navbar expand="lg" bg="primary">

		<Navbar.Brand href="#">Bootcamp</Navbar.Brand>
		<Navbar.Toggle aria-controls ="basic-navbar-nav" />
		<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="me-auto">
				<Nav.Link href="#home">Home</Nav.Link>
				<Nav.Link href="#courses">Courses</Nav.Link>
			</Nav>
		</Navbar.Collapse>

		</Navbar>
	)
}

export default AppNavbar;

/*You can also use 

export default function AppNavbar(){
	return()
}
*/