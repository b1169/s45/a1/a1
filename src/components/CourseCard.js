import {Card,Row, Col, Container, Button} from "react-bootstrap";

function CourseCard(){

	return(

		<Container fluid className = "mb-4">
			<Row>
				<Col xs = {12} md = {4}>

				<Card className="cardHighlights p-3">
			
				  <Card.Body>
				    <Card.Title>Sample Course</Card.Title>
				    <Card.Text>
				      <p>Description:
				      This is a sample course offering</p>
				      <p>Price: Php 40 000</p>

				      <Button>Enroll Now</Button>
				    </Card.Text>
				  </Card.Body>

				</Card>

				</Col>
			</Row>
		</Container>
		)

}


export default CourseCard;