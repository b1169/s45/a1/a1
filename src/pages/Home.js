import {Fragment} from "react";
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';
import CourseCard from './../components/CourseCard';

function Home(){

	return(

    <Fragment>
        <Banner />
        <Highlights />
        <CourseCard />
    </Fragment>
  )

}

export default Home;